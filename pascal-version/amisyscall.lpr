program amisyscall;

{$mode objfpc}{$H+}

uses
  syscall,
  BaseUnix,
  SysUtils;

var
  pid, i, ret : integer;
  sLine : string;
begin

  for i := 1 to 350 do
  begin
    pid := FpFork;
    if (pid > 0) then
    begin
      do_SysCall(i, 0, 0, 0);
      ret := fpgeterrno;

      sLine := Format('syscall: %d => %d | %s', [i, ret, syserrormessage(ret)]);
      Writeln(sLine);
      exit;
      //halt(0);
    end;
  end;
  exit;
end.


